<?php

namespace Modules\Services\Utilities\Models;

/**
 * Class Person.
 */
class Person
{
    /**
     * @var string
     */
    public $numero;
    /**
     * @var string
     */
    public $nombres_apellidos;
    /**
     * @var string
     */
    public $nombres;
    /**
     * @var string
     */
    public $primer_apellido;
    /**
     * @var string
     */
    public $segundo_apellido;
    /**
     * @var string
     */
    public $codigo_verificacion;
    /**
     * @var string
     */
    public $fecha_de_nacimiento;
    /**
     * @var string
     */
    public $sexo; 
}
